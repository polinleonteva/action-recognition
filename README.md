Action Recognition
==============================

Download from the Kinetics 700-2020 dataset a video with classes containing the word dancing 
https://www.deepmind.com/open-source/kinetics

Train a classification model for these videos (3DCNN, CNN-RNN, video transformer, etc.)  _**Branch ViViT_Model**_

Train the model on individual frames and compare _**Branch Naive_model**_

Train a video classification model with a different approach and compare

Project Organization
------------

    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   │   └── kinetics700_2020 <- Contain csv an json meta for dataset 
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    │
    ├── models             <- Chrckpoints for models
    │
    ├── notebooks          <- Jupyter notebooks.
    │          └── naive_model.ipynb <- Naive model in jupyter notebook
    │
    ├── requirements.txt   <- The requirements file `
    └── src                <- Source code for use in this project.
              ├── __init__.py    <- Makes src a Python module
              │
              ├── data           <- Scripts to download or generate data
              │          └── make_dataset.py <- Create Kinetics Dataset
              │          └── dataset_loader.py <- Download Kinetics Dataset
              │
              ├── features       <- Scripts to turn raw data into features for modeling
              │          └── modules.py <- Modules and layers for ViViT model
              │
              └── models         <- Scripts to train models and then use trained models to make predictions
                         └── train_model.py <- Train models script
                         └── ViViT.py <- ViViT architecture
                         └── naive_model.py <- naive model architecture



--------
## Сравнительная таблица
Обучение проводилось на 10 эпохах. Из датасета Kinetics 700_200 было использовано
только 2300 файлов в виду долгого времени скачивания с  YouTube. 

Исходя из полученных данных модели ViViT с архитектурой транформера,
для улучшения качества необходимо обучить на всем датасете. Внизу представлен
процесс обучения, где заметна тенденция на улучшение качества распознавания.

Наивая модель, основанная на архитектуре MobileNet, начала переобучаться уже на 4 эпохе.
Тк модель не использует никакие дополнительные слои для улучшения качества, 
полученный результат ожидаем

(best resuls)

| Model Name  | Accuracy Train | Loss Train | Accuracy Val |
|-------------|----------------|------------|--------------|
| ViViT       | 25.15%         | 2.398      | 6.49%        |
| Naive model | 100%           | 0.0249     | 6.69%        |

Вывод: 

## Данные с обучения ViViT 
Epoch 1/10 \
Training: 100%|██████████| 239/239 [03:35<00:00,  1.11it/s, Loss=2.75, Accuracy=0.0854]\
Train Loss: 2.7472 \
Train Accuracy: 0.0854 \
Testing: 100%|██████████| 239/239 [00:25<00:00,  9.21it/s, Accuracy=0.0523] \
Validation Accuracy: 0.0523 

Epoch 2/10 \
Training: 100%|██████████| 239/239 [03:20<00:00,  1.19it/s, Loss=2.66, Accuracy=0.113] \
Train Loss: 2.6575 \
Train Accuracy: 0.1130 \
Testing: 100%|██████████| 239/239 [00:28<00:00,  8.44it/s, Accuracy=0.0544] \
Validation Accuracy: 0.0544 

Epoch 3/10  \
Training: 100%|██████████| 239/239 [03:20<00:00,  1.19it/s, Loss=2.64, Accuracy=0.128] \
Train Loss: 2.6387 \
Train Accuracy: 0.1276 \
Testing: 100%|██████████| 239/239 [00:30<00:00,  7.81it/s, Accuracy=0.0565] \
Validation Accuracy: 0.0565

Epoch 4/10 \
Training: 100%|██████████| 239/239 [03:25<00:00,  1.16it/s, Loss=2.61, Accuracy=0.138] \
Train Loss: 2.6072 \
Train Accuracy: 0.1376 \
Testing: 100%|██████████| 239/239 [00:30<00:00,  7.87it/s, Accuracy=0.0377] \
Validation Accuracy: 0.0377

Epoch 5/10 \
Training: 100%|██████████| 239/239 [03:27<00:00,  1.15it/s, Loss=2.58, Accuracy=0.138] \
Train Loss: 2.5845 \
Train Accuracy: 0.1382 \
Testing: 100%|██████████| 239/239 [00:29<00:00,  8.12it/s, Accuracy=0.0502] \
Validation Accuracy: 0.0502 

Epoch 6/10 \
Training: 100%|██████████| 239/239 [03:21<00:00,  1.19it/s, Loss=2.55, Accuracy=0.152] \
Train Loss: 2.5504 \
Train Accuracy: 0.1516 \
Testing: 100%|██████████| 239/239 [00:26<00:00,  8.97it/s, Accuracy=0.046] \
Validation Accuracy: 0.0460

Epoch 7/10 \
Training: 100%|██████████| 239/239 [03:31<00:00,  1.13it/s, Loss=2.5, Accuracy=0.178] \
Train Loss: 2.5024 \
Train Accuracy: 0.1783 \
Testing: 100%|██████████| 239/239 [00:31<00:00,  7.56it/s, Accuracy=0.0418] \
Validation Accuracy: 0.0418

Epoch 8/10 \
Training: 100%|██████████| 239/239 [03:32<00:00,  1.12it/s, Loss=2.46, Accuracy=0.199] \
Train Loss: 2.4617 \
Train Accuracy: 0.1985 \
Testing: 100%|██████████| 239/239 [00:29<00:00,  8.11it/s, Accuracy=0.046] \
Validation Accuracy: 0.0460

Epoch 9/10 \
Training: 100%|██████████| 239/239 [03:30<00:00,  1.13it/s, Loss=2.4, Accuracy=0.231] \
Train Loss: 2.3987 \
Train Accuracy: 0.2310 \
Testing: 100%|██████████| 239/239 [00:30<00:00,  7.73it/s, Accuracy=0.0628] \
Validation Accuracy: 0.0628 

Epoch 10/10 \
Training: 100%|██████████| 239/239 [03:34<00:00,  1.11it/s, Loss=2.33, Accuracy=0.251] \
Train Loss: 2.3329 \
Train Accuracy: 0.2515 \
Testing: 100%|██████████| 239/239 [00:27<00:00,  8.57it/s, Accuracy=0.0649] \
Validation Accuracy: 0.0649 

## Обучение Naive model

Epoch 1/10 \
Training: 100%|██████████| 478/478 [10:48<00:00,  1.36s/it, Loss=2.96, Accuracy=0.0748] \
Train Loss: 2.9551 \
Train Accuracy: 0.0748 \
Testing: 100%|██████████| 239/239 [00:51<00:00,  4.64it/s, Accuracy=0.0669] \
Validation Accuracy: 0.0669

Epoch 2/10 \
Training: 100%|██████████| 478/478 [14:46<00:00,  1.86s/it, Loss=1.83, Accuracy=0.497] \
Train Loss: 1.8258 \
Train Accuracy: 0.4969 \
Testing: 100%|██████████| 239/239 [00:49<00:00,  4.82it/s, Accuracy=0.0523] \
Validation Accuracy: 0.0523

Epoch 3/10 \
Training: 100%|██████████| 478/478 [15:21<00:00,  1.93s/it, Loss=0.305, Accuracy=0.974] \
Train Loss: 0.3050 \
Train Accuracy: 0.9744 \
Testing: 100%|██████████| 239/239 [00:49<00:00,  4.82it/s, Accuracy=0.0544] \
Validation Accuracy: 0.0544

Epoch 4/10 \
Training: 100%|██████████| 478/478 [15:22<00:00,  1.93s/it, Loss=0.0249, Accuracy=1] \
Train Loss: 0.0249 \
Train Accuracy: 1.0000 \
Testing: 100%|██████████| 239/239 [00:49<00:00,  4.79it/s, Accuracy=0.0669] \
Validation Accuracy: 0.0669

_**Можно заметить, что точность на трейне стала 100, значит модель начала переобучатся,
поэтому было принято решение остановить обучение**_