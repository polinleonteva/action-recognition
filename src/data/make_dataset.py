import torch
from torch.utils.data import Dataset
import torchvision.transforms as transforms
import cv2

import pandas as pd
import os


class Kinetics700_200(Dataset):
    """
        Dataset from the Kinetics700_200 dataset.
        """

    def __init__(self, df_data, fraction=1.0):
        """
        Initialize the Kinetics700_200 dataset.

        Args:
            df_data (pd.DataFrame): DataFrame containing the dataset information.
            fraction (float, optional): Fraction of the dataset to use. Defaults to 1.0.
        """
        self.df = df_data
        self.kinetic_dataset_video_path = r'D:\work\ActionRec\src\videos'
        self.fraction = fraction
        self.num_frames = 8
        self.unique_classes = self.df['label'].unique()
        self.label_dict = {label: index for index, label in enumerate(self.unique_classes)}
        self.df['target'] = self.df.label.map(self.label_dict)
        self.resize = (112, 112)

        self.transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Resize((112, 112), antialias=True),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
            # Use appropriate normalization values
        ])

    def __len__(self):
        """
        Get the number of samples in the dataset.

        Returns:
            int: Number of samples in the dataset.
        """
        return len(self.df)

    @staticmethod
    def _load_video_frames(video_path):
        cap = cv2.VideoCapture(video_path)
        frames = []
        while True:
            ret, frame = cap.read()
            if not ret:
                break
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frames.append(frame)
        fps = cap.get(cv2.CAP_PROP_FPS)
        cap.release()
        return frames, fps

    def _normalize_frames(self, frames):
        normalized_frames = []
        for frame in frames:
            normalized_frame = self.transform(frame)
            normalized_frames.append(normalized_frame)
        return torch.stack(normalized_frames)

    def _get_sampled_frames(self, frames, fps):
        if len(frames) <= self.num_frames:
            return frames

        frame_rate = fps  # Calculate the frame rate

        if frame_rate * self.num_frames >= len(frames):
            return frames[1:9]

        indices = []
        for i in range(self.num_frames):
            index = int(i * frame_rate)
            indices.append(frames[index])

        return indices

    def __getitem__(self, idx):
        """
        Get a sample from the dataset.

        Args:
            idx (int): Index of the sample.

        Returns:
            tuple: Tuple containing the preprocessed video tensor and the corresponding label.
        """
        df_data = self.df

        class_counts = df_data['label'].value_counts()
        #
        # for label in unique_classes:
        #     count = class_counts[label]
        #     print(f"{label}: {count}")

        if self.fraction < 1:
            selected_samples = []
            for label in self.unique_classes:
                count = class_counts[label]
                num_samples = int(count * self.fraction)
                samples = df_data[df_data['label'] == label].sample(n=num_samples, random_state=42)
                selected_samples.append(samples)

            df_data = pd.concat(selected_samples)
            df_data.reset_index(drop=True, inplace=True)

        frames, fps = self._load_video_frames(os.path.join(self.kinetic_dataset_video_path, self.df['name_video'][idx]))
        frames = self._get_sampled_frames(frames, fps)
        frames = self._normalize_frames(frames)

        x = frames
        # Preprocess images and convert labels to numpy array
        y = torch.tensor(self.df.loc[idx]['target'])
        return x, y


if __name__ == "__main__":
    kinetic_dataset_train_csv_path = r'D:\work\ActionRec\src\data\data.csv'
    df = pd.read_csv(kinetic_dataset_train_csv_path)

    ds = Kinetics700_200(df.reset_index())
    data_loader = torch.utils.data.DataLoader(ds, batch_size=2)
