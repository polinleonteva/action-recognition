import os
import sys
import random

import numpy as np
from colorama import Fore
from torch import nn
from tqdm import tqdm

from src.data.make_dataset import Kinetics700_200
from src.models.ViViT import ViViT

import torch
from torch.utils.data import DataLoader
from sklearn.model_selection import train_test_split

import pandas as pd


def seed_everything(seed):
    """
    Seeds basic parameters for reproducibility of results

    Arguments:
        seed {int} -- Number of the seed
    """
    random.seed(seed)
    os.environ["PYTHONHASHSEED"] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    # torch.backends.cudnn.deterministic = True
    # torch.backends.cudnn.benchmark = False


seed_everything(42)


def train_model(model, train_loader, criterion, optimizer, device):
    """
        Train the model using the provided data loader, criterion, and optimizer.

        Args:
            model (torch.nn.Module): The model to train.
            train_loader (torch.utils.data.DataLoader): Data loader for training data.
            criterion: The loss criterion used for training.
            optimizer: The optimizer used for updating the model's parameters.
            device: The device on which to perform training.

        Returns:
            tuple: A tuple containing the average loss and CER across the training data.
        """

    model.train()
    total_loss = 0.0
    epoch_accuracy = 0.0

    progress_bar = tqdm(train_loader, desc="Training", position=0, leave=True, file=sys.stdout,
                        bar_format="{l_bar}%s{bar}%s{r_bar}" % (Fore.GREEN, Fore.RESET))

    for data, labels in progress_bar:
        data = data.to(device)
        labels = labels.to(device)

        optimizer.zero_grad()

        outputs = model(data)

        loss = criterion(outputs, labels)
        loss.backward()

        optimizer.step()

        total_loss += loss.item()

        accuracy = (outputs.argmax(dim=1) == labels).float().mean()
        # update training loss and accuracy
        epoch_accuracy += accuracy.item()

        progress_bar.set_postfix(
            {"Loss": total_loss / (progress_bar.n + 1), "Accuracy": epoch_accuracy / (progress_bar.n + 1)})

    # Return the computed CER
    return total_loss / len(train_loader), round(epoch_accuracy / len(train_loader), 4)


def test_model(model, test_loader, device):
    model.eval()
    valid_accuracy = 0.0

    progress_bar = tqdm(test_loader, desc="Testing", position=0, leave=True, file=sys.stdout,
                        bar_format="{l_bar}%s{bar}%s{r_bar}" % (Fore.BLUE, Fore.RESET))

    with torch.no_grad():
        for data, labels in progress_bar:
            data = data.to(device)
            labels = labels.to(device)

            outputs = model(data)

            # predicted = outputs.argmax(dim=2)

            accuracy = (outputs.argmax(dim=1) == labels).float().mean()
            # update average validation loss and accuracy
            valid_accuracy += accuracy.item()

            progress_bar.set_postfix({"Accuracy": valid_accuracy / len(test_loader)})

    return round(valid_accuracy / len(test_loader), 4)


def main():
    # Set the device
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    num_epochs = 10
    batch_size = 8
    current_acc = 0

    model_save_path = r"D:\work\ActionRec\models"
    kinetic_dataset_train_csv_path = r'D:\work\ActionRec\src\data\data.csv'

    df = pd.read_csv(kinetic_dataset_train_csv_path)

    n_classes = df['label'].nunique()
    # Split the dataset into train and test sets
    train_df, val_df = train_test_split(df, test_size=0.2, random_state=42, stratify=df['label'])

    # Create train and test datasets
    train_dataset = Kinetics700_200(train_df.reset_index())
    val_dataset = Kinetics700_200(val_df.reset_index())
    # print(train_dataset[0][0].shape, val_dataset[0][0].shape)

    # Create train and test data loaders
    train_data_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True, )
    val_data_loader = torch.utils.data.DataLoader(val_dataset, batch_size=2)

    # CHOOSE YOUR MODEL HERE
    model = ViViT(image_size=112, patch_size=16, num_classes=n_classes, num_frames=train_dataset[0][0].shape[0])

    model.to(device)

    # Define the optimizer and criterion
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
    criterion = nn.CrossEntropyLoss()

    # Training loop

    for epoch in range(num_epochs):
        print(f"Epoch {epoch + 1}/{num_epochs}")

        train_loss, train_accuracy = train_model(model, train_data_loader, criterion, optimizer, device)
        print(f"Train Loss: {train_loss:.4f}")
        print(f"Train Accuracy: {train_accuracy:.4f}")

        val_acc = test_model(model, val_data_loader, device)
        print(f"Validation Accuracy: {val_acc:.4f}")

        # Save the trained model
        if val_acc > current_acc:
            model_out_name = model_save_path + f"/checkpoint_{epoch}_train.pt"
            torch.save(model.state_dict(), model_out_name)


if __name__ == "__main__":
    main()
